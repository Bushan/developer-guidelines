# Developer guidelines

AWS infrastructure setup
------------------------

1. Create Cognito user pools
2. Database:<br>
	Create an RDS instnace of Postgres database<br>
	Create schema and then table.<br>
	Scripts to create the table I have shared with you in my previous emails<br>
3. Create two AWS Lambdas for Login, Save/get Casemix details.
4. Create AWS API Gateways and configure with AWS Lambdas mentioned in step3.
5. Deploying Angular frontend component:- <br>
	Build the angular project by following the steps mentioned in the repository's readme file. <br>
	Create an AWS s3 bucket <br>
	Copy all the files from dist folder into s3 bucket.<br>
	Make the s3 bucket public for now.<br>
	then create static url by selecting the properties tab in the S3.<br>
	This will give a url to access the frontend component.<br>
	Configure the cognito user pool[created in the step-1] in the Angular project.<br>
6. Confgure the new database created in the step 2 in the code(save and get casemix project, in my gitlab the repository name is tcc_casemix)

